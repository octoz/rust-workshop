# Counting words

Utility to count words of a file


## Requirements
[Install RUST](https://www.rust-lang.org/en-US/install.html):

```sh
curl https://sh.rustup.rs -sSf | sh # installer
rustup component add rust-docs # add doc locally
```

## Commands

```sh
rustup doc # open documentation
cargo build # compile
cargo test # run the tests
curl http://www.gutenberg.org/cache/epub/1661/pg1661.txt > sherlock-holmes.txt # download test data
cargo run sherlock-holmes.txt # run the program
```

## IDE

* [ItelliJ Idea](https://github.com/intellij-rust/intellij-rust): You can find it by tying rust in search bar in IntelliJ Idea plugin search
* [Visual studio Code](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust): `ext install rust`
* [Atom](https://atom.io/packages/language-rust): `apm install language-rust`
* Vim: you are on your own

