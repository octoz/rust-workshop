use std::collections::HashMap;
use std::env;
use std::fs::File;
use std::io::prelude::*;

fn main() {
    let filename = env::args().nth(1).expect("please supply an filename");
    let mut f = File::open(filename).expect("file not found");
    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");
    
    println!("All word occurence{:?}", count_word(contents));
    // println!("Most occurence: {:?}", retrieve_most_used_words(contents, 20));
}

fn count_word(text: String) -> HashMap<String, i32> {
    let mut map = HashMap::new();
    let words = text
        .split(|c: char| ! c.is_alphabetic())
        .filter(|w| *w != "")
        .map(|w| w.to_lowercase());
    for word in words {
        let mut count = map.entry(word).or_insert(0);
        *count += 1;
    }
    return map
}

fn retrieve_most_used_words(text:String, limit: usize) -> Vec<(String, i32)> {
    let map = count_word(text);
    let mut entries: Vec<(String, i32)> = map.into_iter().collect();
    entries.sort_by(|a,b| b.1.cmp(&a.1));
    return entries.into_iter().take(limit).collect();
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    #[test]
    fn count_no_word() {
        // given
        let input = "".to_string();

        // when
        let actual = super::count_word(input);

        // then
        let expected: HashMap<String, i32> = HashMap::new();
        assert_eq!(actual, expected);
    }

    #[test]
    fn count_one_word() {
        // given
        let input = "toto".to_string();

        // when
        let actual = super::count_word(input);

        // then
        let mut expected: HashMap<String, i32> = HashMap::new();
        expected.insert("toto".to_string(), 1);
        assert_eq!(actual, expected);
    }

    #[test]
    fn count_words() {
        // given
        let input = "toto titi toto".to_string();

        // when
        let actual = super::count_word(input);

        // then
        let mut expected: HashMap<String, i32> = HashMap::new();
        expected.insert("toto".to_string(), 2);
        expected.insert("titi".to_string(), 1);
        assert_eq!(actual, expected);
    }

    #[test]
    fn count_words_ignore_case() {
        // given
        let input = "toTo titi toto".to_string();

        // when
        let actual = super::count_word(input);

        // then
        let mut expected: HashMap<String, i32> = HashMap::new();
        expected.insert("toto".to_string(), 2);
        expected.insert("titi".to_string(), 1);
        assert_eq!(actual, expected);
    }


    #[test]
    fn count_words_punctuation() {
        // given
        let input = "toTo.titi/toto".to_string();

        // when
        let actual = super::count_word(input);

        // then
        let mut expected: HashMap<String, i32> = HashMap::new();
        expected.insert("toto".to_string(), 2);
        expected.insert("titi".to_string(), 1);
        assert_eq!(actual, expected);
    }

    #[test]
    fn count_words_punctuation_and_space() {
        // given
        let input = "toTo, titi toto".to_string();

        // when
        let actual = super::count_word(input);

        // then
        let mut expected: HashMap<String, i32> = HashMap::new();
        expected.insert("toto".to_string(), 2);
        expected.insert("titi".to_string(), 1);
        assert_eq!(actual, expected);
    }

    #[test]
    fn retrieve_most_used_words() {
        // given
        let input = "toto titi toto titi tutu toto".to_string();

        // when
        let actual = super::retrieve_most_used_words(input, 2);

        // then
        let expected = vec![("toto".to_string(), 3), ("titi".to_string(), 2)];
        assert_eq!(actual, expected);
    }

    
}
